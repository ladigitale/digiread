<?php

session_start();

$env = '../.env';
if (isset($_SESSION['domainesAutorises']) || file_exists($env)) {
	if (isset($_SESSION['domainesAutorises']) && $_SESSION['domainesAutorises'] !== '') {
		$domainesAutorises = $_SESSION['domainesAutorises'];
	} else if (file_exists($env)) {
		$donneesEnv = explode("\n", file_get_contents($env));
		foreach ($donneesEnv as $ligne) {
			preg_match('/([^#]+)\=(.*)/', $ligne, $matches);
			if (isset($matches[2])) {
				putenv(trim($ligne));
			}
		}
		$domainesAutorises = getenv('AUTHORIZED_DOMAINS');
		$_SESSION['domainesAutorises'] = $domainesAutorises;
	}
	if ($domainesAutorises === '*') {
		$origine = $domainesAutorises;
	} else {
		$domainesAutorises = explode(',', $domainesAutorises);
		$origine = $_SERVER['SERVER_NAME'];
	}
	if ($origine === '*' || in_array($origine, $domainesAutorises, true)) {
		header('Access-Control-Allow-Origin: $origine');
		header('Access-Control-Allow-Methods: POST');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');
	} else {
		header('Location: ../');
		exit();
	}
} else {
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Max-Age: 1000');
	header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');
}

if (!empty($_POST['id'])) {
	require 'db.php';
	$id = $_POST['id'];
	$stmt = $db->prepare('SELECT lien, vues FROM digiread_liens WHERE url = :url');
	if ($stmt->execute(array('url' => $id))) {
		if ($donnees = $stmt->fetchAll()) {
			$lien = $donnees[0]['lien'];
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($curl, CURLOPT_URL, $lien);
			curl_setopt($curl, CURLOPT_REFERER, $lien);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:94.0) Gecko/20100101 Firefox/94.0");
			$reponse = curl_exec($curl);
			if (curl_errno($curl)) {
				$reponse = 'erreur';
			}
			curl_close($curl);
			if ($reponse !== 'erreur') {
				$date = date('Y-m-d H:i:s');
				$vues = 0;
				if ($donnees[0]['vues'] !== '') {
					$vues = intval($donnees[0]['vues']);
				}
				$vues = $vues + 1;
				$stmt = $db->prepare('UPDATE digiread_liens SET vues = :vues, derniere_visite = :derniere_visite WHERE url = :url');
				if ($stmt->execute(array('vues' => $vues, 'derniere_visite' => $date, 'url' => $id))) {
					echo json_encode(array('lien' => $lien, 'vues' => $vues, 'reponse' => $reponse));
				} else {
					echo 'erreur';
				}
			} else {
				echo $reponse;
			}
			$db = null;
		} else {
			echo 'contenu_inexistant';
		}
	} else {
		echo 'erreur';
	}
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
